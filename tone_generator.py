#!/usr/bin/env python

def getTone(freq):
    import numpy as np
    fs = 10000.0
    T = 0.05
    nsamples = T * fs
    t = np.linspace(0, T, nsamples, endpoint=False)
    a = 0.02
    x = a * np.cos(2 * np.pi * freq * t + .11)
    return x, t

def main():
    import matplotlib.pyplot as plt
    from scipy.signal import freqz
    import fft_test as ft

    plot_idx = 1
    # Sample rate and desired cutoff frequencies (in Hz).
    x, t = getTone(600)
    plt.figure(plot_idx)
    plot_idx += 1
    plt.clf()
    plt.plot(t, x, label='Noisy signal')
    plt.show()

    y = ft.fft(x)
    ft.plot_fft(t, y)


if __name__ == "__main__":
    main()