import numpy as np


def fft(signal):
    import scipy.fftpack
    return scipy.fftpack.fft(signal)


def plot_fft(xf, yf):
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots()
    ax.plot(xf, np.abs(yf[:xf.shape[0]])/xf.shape[0])
    plt.show()


def main():
    #Creamos la señal de ejemplo
    N = 600  # cantidad de muestras
    T = 1.0 / 600.0  # Tiempo
    x = np.linspace(0.0, N*T, N)  # Vector X
    y = np.sin(50.0 * 2.0*np.pi*x) + 0.5*np.sin(80.0 * 2.0*np.pi*x)  # Vector Y

    yf = fft(y)
    xf = np.linspace(0.0, 1.0/(2.0*T), N/2)

    plot_fft(xf, yf)


if __name__ == "__main__":
    main()